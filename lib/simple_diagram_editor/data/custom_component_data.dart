import 'package:flutter/material.dart';

class MyComponentData {
  Color color;
  Color borderColor;
  double borderWidth;
  String username;

  String text;
  String classproperty;
  Alignment textAlignment;
  double textSize;

  bool isHighlightVisible = false;

  MyComponentData({
    this.color = Colors.white,
    this.borderColor = Colors.black,
    this.borderWidth = 0.0,
    this.text = '',
    this.textAlignment = Alignment.center,
    this.textSize = 20,
    this.username='',
    this.classproperty='',
  });

  MyComponentData.copy(MyComponentData customData)
      : this(
          color: customData.color,
          borderColor: customData.borderColor,
          borderWidth: customData.borderWidth,
          text: customData.text,
          classproperty:customData.classproperty,
          textAlignment: customData.textAlignment,
          textSize: customData.textSize,
          username:customData.username,
        );

  switchHighlight() {
    isHighlightVisible = !isHighlightVisible;
  }

  showHighlight() {
    isHighlightVisible = true;
  }

  hideHighlight() {
    isHighlightVisible = false;
  }
}
