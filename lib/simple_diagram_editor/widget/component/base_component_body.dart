import 'package:auto_size_text/auto_size_text.dart';
import 'package:diagram_editor/diagram_editor.dart';
import 'package:diagram_editor_apps/simple_diagram_editor/data/custom_component_data.dart';
import 'package:flutter/material.dart';


class BaseComponentBody extends StatelessWidget {
  final ComponentData componentData;
  final CustomPainter componentPainter;

  const BaseComponentBody({
    Key key,
    this.componentData,
    this.componentPainter,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {

    final MyComponentData customData = componentData.data;

    return GestureDetector(
      child: Container(
        //decoration: BoxDecoration(
        //border: Border.all(color: Colors.black)),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
             Flexible(
             child: Column(
               crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisAlignment: MainAxisAlignment.start,
               children: [
                 new Container(
                       decoration: BoxDecoration(
                       border: Border.all(color: Colors.black),
                         color: Colors.white,
                      ),
                     child: Text(
                         customData.classproperty,
                         style: TextStyle(fontSize: customData.textSize-7,
                     )),
                 ),

                 new Container(
                   // margin: const EdgeInsets.all(15.0),
                   //padding: const EdgeInsets.all(3.0),
                   //height:componentData.size.height,
                   //width:componentData.size.width-35,
                     decoration: BoxDecoration(
                       border: Border.all(color: Colors.black,width:1.5),
                      color: customData.color,
                   ),
                   child: Text(customData.text,
                           style: TextStyle(fontSize: customData.textSize-7,)
                   ),
                  )
               ],

             ),
           )],
               ),
     )
    );
  }
}
