import 'dart:ui';

import 'package:diagram_editor/diagram_editor.dart';
import 'package:diagram_editor_apps/simple_diagram_editor/data/custom_component_data.dart';
import 'package:diagram_editor_apps/simple_diagram_editor/policy/custom_policy.dart';
import 'package:diagram_editor_apps/simple_diagram_editor/widget/component/rect_component.dart';
import 'dart:developer' as developer;

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'component_design_policy.dart';

mixin MyCanvasPolicy implements CanvasPolicy, CustomStatePolicy {
  @override
  onCanvasTapUp(TapUpDetails details) {
     //ComponentData componentData=getComponentData();
     String componentId = canvasWriter.model.addComponent(
        ComponentData(
          size: Size(150, 38),
          // minSize: Size(80, 64),
              position: canvasReader.state.fromCanvasCoordinates(details.localPosition),
          data: MyComponentData(
            color: Colors.white,
            borderColor: Colors.black,
            borderWidth: 2.0,

          ),
          type: 'rect',
        )
    );
    canvasWriter.model.moveComponentToTheFrontWithChildren(componentId);
    canvasWriter.model.updateComponent(componentId);
    canvasReader.model.getComponent(componentId).data.showHighlight();
    canvasReader.model.getComponent(componentId).updateComponent();
    if (isReadyToConnect) {
      isReadyToConnect = false;
      canvasWriter.model.updateComponent(selectedComponentId);

    } else {
      selectedComponentId = null;
      hideAllHighlights();
    }

    showComponentBody(canvasReader.model.getComponent(componentId));
  }
  @override
  Widget showComponentBody(ComponentData componentData) {
    switch (componentData.type) {
      case 'rect':
        return RectBody(componentData: componentData);
        break;
      case 'body':
        return RectBody(componentData: componentData);
        break;
      default:
        return null;
        break;
    }
  }

}
