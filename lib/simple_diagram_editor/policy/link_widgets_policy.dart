import 'package:diagram_editor/diagram_editor.dart';
import 'package:diagram_editor_apps/simple_diagram_editor/policy/custom_policy.dart';
import 'package:diagram_editor_apps/simple_diagram_editor/dialog/edit_link_dialog.dart';
import 'package:flutter/material.dart';
import 'dart:developer' as developer;


mixin MyLinkWidgetsPolicy implements LinkWidgetsPolicy, CustomStatePolicy {

  @override
  List<Widget> showWidgetsWithLinkData(
      BuildContext context, LinkData linkData) {
    double linkLabelSize = 80;
    var linkMiddleLabelPosition = labelPosition(
      linkData.linkPoints.first,
      linkData.linkPoints.last,
      linkLabelSize / 2,
      false,
    );
    return [
      label(
          linkMiddleLabelPosition,
          linkData.data.startLabel,
          linkLabelSize,linkData.linkStyle.color
      ),
      if (selectedLinkId == linkData.id) showLinkOptions(context, linkData),
    ];
  }

  Widget showLinkOptions(BuildContext context, LinkData linkData) {
    var nPos = canvasReader.state.toCanvasCoordinates(tapLinkPosition);
    return Positioned(
      left: nPos.dx,
      top: nPos.dy,
      child: Row(
        children: [
          GestureDetector(
            onTap: () {
              canvasWriter.model.removeLink(linkData.id);
            },
            child: Container(
                decoration: BoxDecoration(
                  color: Colors.red.withOpacity(0.7),
                  shape: BoxShape.circle,
                ),
                width: 32,
                height: 32,
                child: Center(child: Icon(Icons.close, size: 20))),
          ),
          SizedBox(width: 8),
          GestureDetector(
            onTap: () {
              showEditLinkDialog(
                context,
                linkData,
              );
            },
            child: Container(
                decoration: BoxDecoration(
                  color: Colors.grey.withOpacity(0.7),
                  shape: BoxShape.circle,
                ),
                width: 32,
                height: 32,
                child: Center(child: Icon(Icons.edit, size: 20))),
          ),
        ],
      ),
    );
  }

  Widget label(Offset position, String label, double size,Color colour) {

    return Positioned(



      left: position.dx,
      top: position.dy,
      width: size * canvasReader.state.scale,
      height: size * canvasReader.state.scale,
      child: Container(
        //color: Colors.cyan,
        child: GestureDetector(
          onTap: () {},
          onLongPress: () {},
          child: Center(
            child: Text(
              label,
              style: TextStyle(
                fontSize: 15* canvasReader.state.scale,
                backgroundColor: Colors.grey,
                color:colour,
              ),
            ),
          ),
        ),
      ),
    );
  }

  Offset labelPosition(
      Offset point1,
      Offset point2,
      double labelSize,
      bool left,
      ) {
    var normalized = VectorUtils.normalizeVector((point2 - point1)/2);
    double x1 =  point1.dx;
    double y1 =  point1.dy;
    double x2 =  point2.dx;
    double y2 =  point2.dy;

    double x  = x1+((x2-x1)/2)-26;
    double y = y1+((y2-y1)/2)-26;
    Offset middlePoint=Offset (x.abs(),y.abs());
    //return middlePoint;
    return canvasReader.state.toCanvasCoordinates(middlePoint);
  }

// @override
// Widget showOnLinkTapWidget(
//     BuildContext context, LinkData linkData, Offset tapPosition) {
//   return Positioned(
//     left: tapPosition.dx,
//     top: tapPosition.dy,
//     child: Row(
//       children: [
//         GestureDetector(
//           onTap: () {
//             canvasWriter.model.removeLink(linkData.id);
//           },
//           child: Container(
//               decoration: BoxDecoration(
//                 color: Colors.red.withOpacity(0.7),
//                 shape: BoxShape.circle,
//               ),
//               width: 32,
//               height: 32,
//               child: Center(child: Icon(Icons.close, size: 20))),
//         ),
//         SizedBox(width: 8),
//         GestureDetector(
//           onTap: () {
//             showEditLinkDialog(
//               context,
//               linkData,
//             );
//           },
//           child: Container(
//               decoration: BoxDecoration(
//                 color: Colors.grey.withOpacity(0.7),
//                 shape: BoxShape.circle,
//               ),
//               width: 32,
//               height: 32,
//               child: Center(child: Icon(Icons.edit, size: 20))),
//         ),
//       ],
//     ),
//   );
// }
}
