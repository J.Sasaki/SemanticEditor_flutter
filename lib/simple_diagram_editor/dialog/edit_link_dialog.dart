import 'package:diagram_editor/diagram_editor.dart';
import 'package:diagram_editor_apps/simple_diagram_editor/data/custom_link_data.dart';
import 'package:diagram_editor_apps/simple_diagram_editor/dialog/pick_color_dialog.dart';
import 'package:flutter/material.dart';

void showEditLinkDialog(BuildContext context, LinkData linkData) {
  MyLinkData customData = linkData.data;

  Color color = linkData.linkStyle.color;
  LineType lineTypeDropdown = linkData.linkStyle.lineType;
  double lineWidthPick = linkData.linkStyle.lineWidth;

  ArrowType arrowTypeDropdown = linkData.linkStyle.arrowType;
  double arrowSizePick = linkData.linkStyle.arrowSize;
  ArrowType backArrowTypeDropdown = linkData.linkStyle.backArrowType;
  double backArrowSizePick = linkData.linkStyle.backArrowSize;

  bool isLabelsEditShown = false;
  bool isColorEditShown = false;


  final startLabelController =
      TextEditingController(text: customData.startLabel ?? '');

  showDialog(
    barrierDismissible: false,
    useSafeArea: true,
    context: context,
    builder: (BuildContext context) {
      return StatefulBuilder(
        builder: (context, setState) {
          return AlertDialog(
            content: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(width: 600),
                Text('Edit link', style: TextStyle(fontSize: 20)),
                Divider(color: Colors.black, height: 8, thickness: 1),
                ShowItem(
                    text: 'Text  color',
                    isShown: isColorEditShown,
                    onTap: () =>
                        setState(() => isColorEditShown = !isColorEditShown)),
                Visibility(
                  visible: isColorEditShown,
                  child: Row(
                    children: [
                      Text('Color:'),
                      SizedBox(width: 16),
                      GestureDetector(
                        onTap: () async {
                          var pickedColor =
                          // showPickColorDialog(context, linkData);
                          showPickColorDialog(
                              context, color, 'Pick a text color');
                          linkData.linkStyle.color = await pickedColor;
                          setState(() {});
                        },
                        child: Container(
                          width: 40,
                          height: 40,
                          decoration: BoxDecoration(
                            color: linkData.linkStyle.color,
                            shape: BoxShape.circle,
                            border: Border.all(color: Colors.black),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 8),
                ShowItem(
                    text: 'Link labels',
                    isShown: isLabelsEditShown,
                    onTap: () =>
                        setState(() => isLabelsEditShown = !isLabelsEditShown)),
                Visibility(
                  visible: isLabelsEditShown,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      TextField(
                        controller: startLabelController,
                        maxLines: 1,
                        decoration: InputDecoration(
                          labelText: 'Start label',
                          fillColor: Colors.white,
                          contentPadding: EdgeInsets.all(25),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
            scrollable: true,
            actions: [
              TextButton(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: Text('DISCARD'),
              ),
              TextButton(
                onPressed: () {
                  customData.startLabel = startLabelController.text;
                   linkData.updateLink();
                  Navigator.of(context).pop();
                },
                child: Text('SAVE'),
              )
            ],
          );
        },
      );
    },
  );
}

class ShowItem extends StatelessWidget {
  final String text;
  final bool isShown;
  final Function onTap;

  const ShowItem({Key key, this.text, this.isShown, this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => onTap(),
      child: Row(
        children: [
          Text(text),
          Icon(isShown ? Icons.keyboard_arrow_up : Icons.keyboard_arrow_down),
        ],
      ),
    );
  }
}
