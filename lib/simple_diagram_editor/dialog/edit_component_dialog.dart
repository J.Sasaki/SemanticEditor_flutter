import 'package:diagram_editor/diagram_editor.dart';
import 'package:diagram_editor_apps/simple_diagram_editor/data/custom_component_data.dart';
import 'package:diagram_editor_apps/simple_diagram_editor/dialog/edit_link_dialog.dart';
import 'package:diagram_editor_apps/simple_diagram_editor/dialog/pick_color_dialog.dart';
import 'package:diagram_editor_apps/simple_diagram_editor/policy/component_widgets_policy.dart';
import 'package:flutter/material.dart';


import 'dart:developer' as developer;

 void showEditComponentDialog (
    BuildContext context, ComponentData componentData){
  MyComponentData customData = componentData.data;
   MyComponentWidgetsPolicy componentPolicy;
  Color color = customData.color;
  Color borderColor = customData.borderColor;

  double borderWidthPick = customData.borderWidth;
  double maxBorderWidth = 40;
  double minBorderWidth = 0;
  double borderWidthDelta = 0.1;

  final textController = TextEditingController(text: customData.text ?? '');
  final textControllerClassProperty = TextEditingController(text: customData.classproperty ?? '');


  Alignment textAlignmentDropdown = customData.textAlignment;
  var alignmentValues = [
    Alignment.topLeft,
    Alignment.topCenter,
    Alignment.topRight,
    Alignment.centerLeft,
    Alignment.center,
    Alignment.centerRight,
    Alignment.bottomLeft,
    Alignment.bottomCenter,
    Alignment.bottomRight,
  ];
  double textSizeDropdown = customData.textSize;
  var textSizeValues =
      List<double>.generate(20, (int index) => index * 2 + 10.0);
  showDialog(
    barrierDismissible: false,
    useSafeArea: true,
    context: context,
    builder: (BuildContext context) {
      return StatefulBuilder(builder: (context, setState) {
        return AlertDialog(
          content: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(width: 600),
              Text('Edit component', style: TextStyle(fontSize: 20)),
              SizedBox(height: 8),
              SizedBox(height: 16),
              TextField(
                controller: textControllerClassProperty,
                maxLines: 1,
                decoration: InputDecoration(
                  labelText: 'Class and Property',
                  fillColor: Colors.white,
                ),
              ),
              TextField(
                controller: textController,
                maxLines: 1,
                decoration: InputDecoration(
                  labelText: 'Text',
                  fillColor: Colors.white,
                ),
              ),
              SizedBox(height: 8),
              SizedBox(height: 16),
              Row(
                children: [
                  Text('Component color:'),
                  SizedBox(width: 16),
                  GestureDetector(
                    onTap: () async {
                      var pickedColor = showPickColorDialog(
                          context, color, 'Pick a component color');
                      color = await pickedColor;
                      setState(() {});
                    },
                    child: Container(
                      width: 40,
                      height: 40,
                      decoration: BoxDecoration(
                        color: color,
                        shape: BoxShape.circle,
                        border: Border.all(color: Colors.grey),
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(height: 8),
              SizedBox(height: 8),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
              ),
            ],
          ),
          scrollable: true,
          actions: [
            TextButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: Text('DISCARD'),
            ),
            TextButton(
              onPressed: () {
                customData.text = textController.text;
                customData.classproperty = textControllerClassProperty.text;
                customData.textAlignment = textAlignmentDropdown;
                customData.textSize = textSizeDropdown;
                customData.color = color;
                customData.borderColor = borderColor;
                customData.borderWidth = borderWidthPick;
                int length= textController.text.length + textControllerClassProperty.text.length;
                double height=0;
               double lineCount=(length/18).roundToDouble();
                 switch (lineCount.toString()) {
                  case '1.0':
                   height= 38;
                   break;
                  case '2.0':
                    height=38;
                    break;
                  case '3.0':
                    height=53;
                    break;
                  case '4.0':
                    height=67;
                    break;
                  case '5.0':
                    height=82;
                    break;
                  case '6.0':
                    height=97;
                    break;
                  case '7.0':
                    height=100;
                    break;
                  case '8.0':
                    height=117;
                    break;
                  case '9.0':
                    height=125;
                    break;
                  case '10.0':
                    height=158;
                    break;
                  case '11.0':
                    height=140;
                    break;
                  case '12.0':
                    height=168;
                    break;
                  case '13.0':
                    height=175;
                    break;
                  case '14.0':
                    height=172;
                    break;
                  case '15.0':
                    height=185;
                    break;
                  default:
                    height=195;
                    break;
                }
                 componentData.setSize(Size(150,height));
                componentData.updateComponent();
                 Navigator.of(context).pop();
            },
              child: Text('SAVE'),

            )
          ],
        );
      });
    },
  );
}
